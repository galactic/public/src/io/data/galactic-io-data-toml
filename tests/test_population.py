"""TOML population test module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.toml import TOMLDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, TOMLDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
