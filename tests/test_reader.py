"""TOML reader test module."""

from pathlib import Path
from unittest import TestCase

from galactic.io.data.core import PopulationFactory


class TomlDataReaderTest(TestCase):
    def test_get_reader(self):
        pathname = Path(__file__).parent / "test.toml"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "individual1": {"name": "Galois", "firstname": "Évariste"},
                "individual2": {"name": "Wille", "firstname": "Rudolf"},
            },
        )
