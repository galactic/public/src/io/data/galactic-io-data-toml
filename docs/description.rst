==================
*TOML* data reader
==================

*galactic-io-data-toml* is a
`TOML <https://en.wikipedia.org/wiki/TOML>`_  data reader plugin
for **GALACTIC**.

The file extension is ``.toml``. The individuals are represented in named
sections by `key = "value"` pairs. For example:

.. code-block:: ini
    :class: admonition

    [individual1]
        name = "Galois"
        firstname = "Évariste"
    [individual2]
        name = "Wille"
        firstname = "Rudolf"

This reader uses the ``toml.load`` function of the
`toml library <https://pypi.org/project/toml/#api-reference>`_.

