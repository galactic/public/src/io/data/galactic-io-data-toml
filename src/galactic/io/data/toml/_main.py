"""
TOML Data reader.
"""

from collections.abc import Iterator, Mapping
from typing import TextIO, cast

import toml
from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class TOMLDataReader:
    """
    `̀ TOML`̀  Datareader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.toml import TOMLDataReader
    >>> reader = TOMLDataReader()
    >>> import io
    >>> data = '''# This is a TOML document.
    ... [individual1]
    ...     name="Galois"
    ...     firstname="Évariste"
    ... [individual2]
    ...     name="Wille"
    ...     firstname="Rudolf"
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> pprint(individuals)
    {'individual1': {'firstname': 'Évariste', 'name': 'Galois'},
     'individual2': {'firstname': 'Rudolf', 'name': 'Wille'}}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``TOML`` data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        """
        return dict(toml.load(data_file))

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".toml"])


def register() -> None:
    """
    Register an instance of a ``TOML`` data reader.
    """
    PopulationFactory.register_reader(TOMLDataReader())
