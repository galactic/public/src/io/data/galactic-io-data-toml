"""
TOML Data reader.
"""

from ._main import TOMLDataReader, register

__all__ = ("TOMLDataReader", "register")
