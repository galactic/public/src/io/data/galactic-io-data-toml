Install the **GALACTIC** io data toml package
=============================================

Prerequisite
------------

*galactic-io-data-toml* requires `python 3.10`_,
a programming language that comes pre-installed on linux and Mac OS X,
and which is easily installed `on Windows`_;

Installation
------------

Install *galactic-io-data-toml* using the bash command

.. code-block:: shell-session

    $ pip install \
        --index-url https://gitlab.univ-lr.fr/api/v4/groups/galactic/-/packages/pypi/simple \
        galactic-io-data-toml[docs]

Don't forget to add the ``--pre`` flag if you want the latest unstable build.

Build
-----

Building *galactic-io-data-toml* requires

* `hatch`_, which is a tool for dependency management and packaging in Python;

Build *galactic-io-data-toml* using the bash command

.. code-block:: shell-session

    $ hatch build

Documentation
-------------

Build the documentation using the bash commands:

.. code-block:: shell-session

    $ hatch run docs:build

Testing
-------

Test *galactic-io-data-toml* using the bash command:

.. code-block:: shell-session

    $ hatch test

for running the tests.

.. code-block:: shell-session

    $ hatch test --cover

for running the tests with the coverage.

.. code-block:: shell-session

    $ hatch test --doctest-modules src

for running the `doctest`.

Linting
-------

Lint *galactic-io-data-toml* using the bash commands:

.. code-block:: shell-session

    $ hatch fmt --check

for running static linting.

.. code-block:: shell-session

    $ hatch fmt

for automatic fixing of static linting issues.

.. code-block:: shell-session

    $ hatch run lint:check

for running dynamic linting.

Getting Help
------------

.. important::

    If you have any difficulties with *galactic-io-data-toml*, please feel welcome to
    `file an issue`_ on gitlab so that we can help.

.. _file an issue: https://gitlab.univ-lr.fr/galactic/public/src/io/data/galactic-io-data-toml/-/issues
.. _python 3.10: http://www.python.org
.. _on Windows: https://www.python.org/downloads/windows
.. _hatch: https://hatch.pypa.io/
